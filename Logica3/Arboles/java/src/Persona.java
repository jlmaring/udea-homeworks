public class Persona{
    private int identificacion;
    private String nombre;
    private String gen1Ojos;
    private String gen2Piel;
    private String gen3Cabello;
    private String gen4Raza;
    private String identificacionPadre;

    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGen1Ojos() {
        return gen1Ojos;
    }

    public void setGen1Ojos(String gen1Ojos) {
        this.gen1Ojos = gen1Ojos;
    }

    public String getGen2Piel() {
        return gen2Piel;
    }

    public void setGen2Piel(String gen2Piel) {
        this.gen2Piel = gen2Piel;
    }

    public String getGen3Cabello() {
        return gen3Cabello;
    }

    public void setGen3Cabello(String gen3Cabello) {
        this.gen3Cabello = gen3Cabello;
    }

    public String getGen4Raza() {
        return gen4Raza;
    }

    public void setGen4Raza(String gen4Raza) {
        this.gen4Raza = gen4Raza;
    }

    public String getIdentificacionPadre() {
        return identificacionPadre;
    }

    public void setIdentificacionPadre(String identificacionPadre) {
        this.identificacionPadre = identificacionPadre;
    }


}