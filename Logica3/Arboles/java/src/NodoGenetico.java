public class NodoGenetico{
    private int switche;
    private NodoGenetico ligaDerecha;
    private NodoGenetico ligaIzquierda;
    private NodoGenetico ligaAlPadre;
    private NodoGenetico factorBalace;
    private int identificacion;
    private Object persona;

    public int getSwitche() {
        return switche;
    }

    public void setSwitche(int switche) {
        this.switche = switche;
    }

    public NodoGenetico getLigaDerecha() {
        return ligaDerecha;
    }

    public void setLigaDerecha(NodoGenetico ligaDerecha) {
        this.ligaDerecha = ligaDerecha;
    }

    public NodoGenetico getLigaIzquierda() {
        return ligaIzquierda;
    }

    public void setLigaIzquierda(NodoGenetico ligaIzquierda) {
        this.ligaIzquierda = ligaIzquierda;
    }

    public NodoGenetico getLigaAlPadre() {
        return ligaAlPadre;
    }

    public void setLigaAlPadre(NodoGenetico ligaAlPadre) {
        this.ligaAlPadre = ligaAlPadre;
    }

    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Object persona) {
        this.persona = persona;
    }

    public NodoGenetico getFactorBalace() {
        return factorBalace;
    }

    public void setFactorBalace(NodoGenetico factorBalace) {
        this.factorBalace = factorBalace;
    }
}